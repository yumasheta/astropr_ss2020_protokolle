# Observation Protocol

### 1. good telescope
#### a) milky way scan

setting the start Glon to anything with 0.x and 360 results in no image.  
so the chosen settings are 1 to 359, the area 359 to 1 will not be mapped.  
the simulator and spectrometer settings are saved as images.

folder: milchstrasse/vlsr_scan  
- simulator settings: `simulation_gutes_teleskop.png`
- spectrometer settings: `spektrometer_vlsr.png`
- observation settings: `vlsr_scan_gutes_teleskop_settings.png`
- image: `vlsr_bild_gutes_teleskop.pdf` and `vlsr_bild_gutes_teleskop.dat`

#### b) point source spectrum
coordinates:  
GLAT: 0  
GLONG: 40  

folder: milchstrasse/punktquelle  
- simulator settings: `simulation_gutes_teleskop.png`
- image: `point_source_good_telescope.png` and `point_source_good_telescope.dat`
- comparison with bad telescope `point_source_comparison.pdf`


### 2. bad telescope
#### a) milky way scan
same problems as before with GLON near 0

folder: milchstrasse/vlsr_scan  
- simulator settings: `simulatior_data_for_srt.png`
- spectrometer settings: `spektrometer_vlsr.png` (same as bfore)
- observation settings: `vlsr_scan_bad_tel_settings.png`
- image: `vlsr_bild_bad_teleskop.pdf` and `vlsr_bild_bad_teleskop.dat`

#### b) point source:
coordinates:  
GLAT: 0  
GLONG: 40  

folder: milchstrasse/punktquelle  
- simulator settings: `point_source_bad_tel_sim_settings.png`
- image: `point_source_bad_telescope.dat`
- comparison `point_source_comparison.pdf`


### 3. planning observation with SRT
#### a) Rotation of galactic plane

folder: milchstrasse/srt_plan

diese spektren sollen rot/blauverschiebung der galaxie und damit deren rotation zeigen  
- simulator settings: `simulator_data_for_srt.png`
- spectrometer settings: same as vlsr scan: `spektrometer_vlsr.png`
- sky view zum zeitpunkt der geplanten observation: `SRT_plan_sky_view.png`
- sky view des Teleskops, damit beobachtungsgrenzen sichtbar sind: `SRT_plan_sky_view_live.png`
- spektrum an Glat=0, GLon im namen:
    - G150: `SRT_glon_150.dat`
    - G180: `SRT_glon_180.dat`
    - G210: `SRT_glon_210.dat`
- vgl der spektren: `SRT_210-180-150_comp.pdf`  
  farben: 210 - rot | 180 - gelb | 150 - blau

#### b) spiralarme
diese spektren sollen die spiralarmstruktur zeigen  
ein peak spaltet sich auf in 2 peaks = arme  
- spektrum an Glat=0, Glon im namen:
    - `SRT_glon_130.dat`
    - `SRT_glon_140.dat`
    - `SRT_glon_150.dat`
    - `SRT_glon_160.dat`
    - `SRT_glon_170.dat`
- vlg der spektren: `SRT_plan_arme_comp.pdf`  
  farben: 170 - rot | 160 - orange | 150 - gelb | 140 - grün | 130 - blau


### 3. Observation with SRT
- spectrometer settings: `SRT_obs_spectrometer.png`
- spektren:
    - `SRT_obs_glon_130.dat`
    - `SRT_obs_glon_140.dat`
    - `SRT_obs_glon_150.dat`
    - `SRT_obs_glon_160.dat`
    - `SRT_obs_glon_170.dat`
    - `SRT_obs_glon_180.dat`
    - `SRT_obs_glon_210.dat`
- vgl 3 spektren: `SRT_obs_210-180-150_comp.pdf`  
  farben: 210 - rot | 180 - gelb | 150 - blau
  
- vgl 5 spektren: `SRT_obs_arme_comp.pdf`  
  farben: 170 - rot | 160 - orange | 150 - gelb | 140 - grün | 130 - blau


### 4. Magellansche wolken

folder MC/  
- simulator settings: `simulator_data_MC.png`

#### a) spektren / doppler-messung

fuer auswertung:
- spektrum LMC mit doppler messung: `spectrum_LMC.png`
- spektrum SMC mit doppler messung: `spectrum_SMC.png`

als bilder im protokoll:
- beide spektren fuer gutes teleskop: `spectra_MC_comp.pdf`
- beide spektren fuer bad teleskop: `spectra_MC_comp_bad_tel.pdf`

#### b) bild mit npoint scan

- settings fuer gutes teleskop: `MC_npoint_settings.png`
- settings fuer schlechtes teleskop: `MC_npoint_settings_bad_telescope.png`
- bild mit gutem teleskop: `MC_npoint_image.dat` und `MC_npoint_image.pdf`
- bild mit bad teleskop: `MC_npoint_image_bad_telescope.pdf`

