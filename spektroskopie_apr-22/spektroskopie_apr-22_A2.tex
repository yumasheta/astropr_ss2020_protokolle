% options for aa style
%
%\documentclass[referee]{aa} % for a referee version
%\documentclass[onecolumn]{aa} % for a paper on 1 column  
%\documentclass[longauth]{aa} % for the long lists of affiliations 
%\documentclass[letter]{aa} % for the letters 
%\documentclass[bibyear]{aa} % if the references are not structured 
%                              according to the author-year natbib style

\documentclass[utf8]{../aa-package/aa}

\usepackage{graphicx}
\usepackage[varg]{txfonts}

%\usepackage[options]{hyperref}
% To add links in your PDF file, use the package "hyperref"
% with options according to your LaTeX or PDFLaTeX drivers.
%
% hidelinks removes red border from clickable links
\usepackage[hidelinks]{hyperref}

% taken from the lecture slides, ''lebensnotwendig'':
% the rest is already in the aa.cls style definition
\usepackage{lmodern}

% use external bib file with aa citation style
\usepackage{natbib}
\bibpunct{(}{)}{;}{a}{}{,} % to follow the A&A style
\bibliographystyle{../aa-package/bibtex/aa}
\usepackage{bibentry}

% generate some text
\usepackage[math]{blindtext}
\usepackage{lipsum}

\begin{document} 

% set the language
% [english , ngerman], default is english
\selectlanguage{ngerman}


   \title{Astronomisches Praktikum --- Spektroskopie}

   \subtitle{Bestimmung der Systemparameter für spektroskopische Doppelsterne}

   \author{Antonia Magenheim (Mat.Nr.: 11711025)
          \inst{\ref{antonia_inst}}
          \and
          Kayran Schmidt (Mat.Nr.: 01604789)
          \inst{\ref{kayran_inst}}
          }

   \institute{
   	     Institute for Astronomy (IfA), University of Vienna, Bachelor of Science Programme 033 661, \\
   	     Course 280445 [PR PM-AstroPR] Astronomical Laboratory Exercises for Beginners \\
              \email{\href{mailto:a11711025@unet.univie.ac.at}{a11711025@unet.univie.ac.at}}\label{antonia_inst}
        \and
              Institute for Astronomy (IfA), University of Vienna, Bachelor of Science Programme 033 661, \\
   	     Course 280445 [PR PM-AstroPR] Astronomical Laboratory Exercises for Beginners \\
              \email{\href{mailto:a01604789@unet.univie.ac.at}{a01604789@unet.univie.ac.at}}\label{kayran_inst}
             }
   \authorrunning{Antonia Magenheim\and Kayran Schmidt}
   \date{22. April 2020}

% \abstract{}{}{}{}{} 
% 5 {} token are mandatory

% it is possible to leave {} empty if optional 
  \abstract
  % context heading (optional)
   {Diese Ausarbeitung wurde im Rahmen des ``Astronomischen Praktikums'' erstellt und gibt einen Einblick in die Analyse von Spektren.
   Das untersuchte Spektrum stammt von einem spektroskopischen Doppelstern.}
  % aims heading (mandatory)
   {Ziel ist es über die Dopplerverschiebung des Na D dupletts die Systemparameter des Doppelsternsystems zu berechnen.
   Diese sind die linearen Radialgeschwindigkeiten und Bahnradien der beiden Sterne, sowie deren Massen und die Periode und relative Systemgeschwindigkeit.}
  % methods heading (mandatory)
   {Die Positionen der Na D Linien wurden durch einen Lorentz-Fit bestimmt.
   Die periodische Veränderung der Radialgeschwindigkeiten wurde durch eine Sinuskurve parametrisiert und durch einen weitern Fit festgelegt.
   Anschließend wurden Methoden der Himmelsmechanik verwendet um mit einem Schätzwert für die Masse des Primärsterns die Systemparameter zu berechnen.}
  % results heading (mandatory)
   {Die erhaltenen Ergebnisse scheinen plausibel, wenngleich sie nur eine grobe Schätzung darstellen.
   Wichtige Aspekte, wie die Inklination der Rotationsebene wurden nicht berücksichtigt.
   Das Auflösungsvermögen der vorliegenden Daten würde nicht ausreichen, um einen Gasriesen anstelle des Sekundärsterns nachzuweisen, da die durch die geringere Masse des Planeten deutlich schwächere Dopplerverschiebung nicht mehr aufgelöst werden kann.}
  % conclusions heading (optional), leave it empty if necessary 
   {}

 % standard abstract with no structured sections takes only one argument
 %\abstract{\lipsum[1]}

% seperate by '--'
   \keywords{Spektroskopischer Doppelstern -- Systemparameter -- Radialgeschwindigkeit -- \emph{scipy.optimize.curve\_fit}}

   \maketitle
%
%-------------------------------------------------------------------

\section{Einleitung}\label{sec:intro}

In dieser Ausarbeitung im Rahmen des ``Spektroskopie Praktikums'' wurde ein spektroskopischer Doppelstern untersucht.
Als Doppelstern (oder Doppelsternsystem) werden Sterne bezeichnet, die sich im Raum so nahe zueinander befinden, dass sie gravitativ gebunden sind.
Dabei ist zu beachten, dass Sterne, die einer Sichtlinie sehr nahe kommen, bei Beobachtungen als Doppelsterne (``scheinbare'' Doppelsterne) erscheinen, obwohl sie nicht im selben Gravitationssystem sind.
Solche Objekte sind im Weiteren uninteressant.

Es gibt verschiedene Techniken Doppelsternsysteme zu entdecken.
Zum einen kann es möglich sein, die beiden Sterne direkt aufzulösen.
Ist dies nicht der Fall, kann ein Doppelstern über das Spektrum seines Lichts identifiziert werden (``spektroskopischer'' Doppelstern).
Wird eine Zeitserie des Spektrums eines entsprechenden Doppelsterns erstellt, so zeigt sich, dass die Lage der Linien in diesem Spektrum sich periodisch verändert.
Dies lässt sich mithilfe des Dopplereffekts erklären.
Da beide Sterne gravitativ gebunden sind, umkreisen sie den gemeinsamen Massenschwerpunkt.
Dabei vollziehen beide Sterne eine periodische Bewegung und ihre beobachteten Bahngeschwindigkeiten verändern sich mit derselben Periode.
Nach der Theorie des Dopplereffekts verschiebt sich dabei die beobachtete Wellenlänge um
\begin{equation}\label{eq:doppler}
   \Delta \lambda = \frac{\Delta v \lambda}{\mathrm{c}},
\end{equation}
wobei $\Delta v$ die relative Geschwindigkeit zum Objekt und $\lambda$ die beobachtete Wellenlänge im Ruhesystem ist.
In der vorliegenden Ausarbeitung wurde dieser Effekt ausgenutzt.

Die zur Verfügung gestellten Spektren haben ein Auflösungsvermögen $R$ von $393003.33$, das sich nach $R = \lambda / \Delta\lambda$ berechnen lässt.
Die Werte für $\lambda$ und $\Delta\lambda$ wurden dem FITS-Header der Spektren entnommen.
Für $\lambda$ wurde der Wert des Referenzpixels $5895.05\AA$ und für $\Delta\lambda$ die spektrale Auflösung pro Pixel $0.015\AA$ eingesetzt.

Der Spektraltyp des Primärsterns is ebenfalls im FITS-Header als F0V eingetragen.
Der Spektraltyp des Sekundärsterns lässt sich aus dem Spektrum ablesen.
Es weist eine starke Mg Linie bei $\approx5170\AA$ auf, die untypisch für F Sterne ist und so höchstwahrscheinlich auf den Sekundärstern zurückzuführen ist.
Sie ist typisch für Sterne des Spektraltyps K/M.

Die nun folgenden Methoden und Ergebnisse wurden in einem Jupyter-Notebook\footnote{\emph{A2\_Spektroskopie\_GR10.ipynb}} ausgearbeitet, das zur Durchsicht beigelegt ist.

\section{Fitten der Absorptionslinien}\label{sec:line-fit}

Ziel ist es in den Spektren die Verschiebung der Linien zu vermessen und über Formel~\ref{eq:doppler} auf $\Delta v$ zu kommen. Zusammen mit der Periode lassen sich dann die Systemparameter des Doppelsterns abschätzen (Abschnitt~\ref{sec:param-fit}).

Als erster Schritt wurde die Zeitserie mit dem Programm \emph{ImageJ}\footnote{\url{https://imagej.net/Welcome}} geöffnet und animiert.
Dies erlaubt es die Verschiebung der Linien visuell zu inspizieren.
Es zeigt sich, dass das Na D duplett besonders stark ausgeprägt ist und sich gut für die Messung der Verschiebung eignet.
Mithilfe von \emph{ImageJ} wurden anschließend von jedem Spektrum der Zeitserie die Intensitätswerte in einem Ausschnitt um das Na D duplett extrahiert und abgespeichert.
Werden diese Werte über einer Pixelskala aufgetragen, ergibt sich ein Spektrum wie in Abbildung~\ref{fig:spektrum}.

\begin{figure}
   \centering
   \includegraphics[width=\hsize,clip,trim = 0 0 0 3350]{A2_spektren.png}
      \caption{Ein Spektrum der Zeitserie. Die normierten Intensitätswerte auf der Y-Achse sind über den sortierten Pixeln aufgetragen.}
      \label{fig:spektrum}
\end{figure}

Die zwei ausgeprägten Minima bei $\sim 100$ und $\sim 500$ sind das Na D duplett.
Hervorzuheben ist auch der deutliche Peak bei $\sim 460$.
Das ist ein Referenzwert, der von den Messinstrumenten stammt und es erlaubt den absoluten Wert der Verschiebung auszurechnen (siehe Abschnitt~\ref{sec:param-fit}).

Dafür müssen zunächst die Pixelpositionen der beiden Na D Linien sowie des Referenzwertes für jedes Spektrum der Zeitserie bestimmt werden.
Die Form der Na D Linien folgt einer Lorentz-Kurve (siehe Gl.~\ref{eq:lorentz}) und es bietet sich an eine entsprechende Kurve an jede der Linien zu fitten.
Aus den Fit-Parametern lassen sich dann die genauen Positionen der Linien ablesen.
Für jedes Spektrum der Zeitserie müssen zuerst die Bereiche identifiziert werden, für die eine Lorentz-Kurve berechnet werden soll.
Der dafür verwendete Algorithmus identifiziert zuerst den Pixel des tiefsten Minimums im Spektrum.
Anschließend wird ein Bereich von $\pm70$px um das Minimum für den Fit markiert.
Für die zweite Na D Linie wird der Vorgang wiederholt, wobei der Bereich der ersten Linie nicht miteinbezogen wird.
Die so für ein Spektrum bestimmten Randwerte der Bereiche sind in Abbildung~\ref{fig:fit} mit Schwarz markiert.

Anschließend wurde die \emph{scipy.optimize.curve\_fit}\footnote{\url{https://docs.scipy.org/doc/scipy-0.18.1/reference/generated/scipy.optimize.curve\_fit.html}} Funktion verwendet um eine Lorentz-Kurve, wie in Gleichung~\ref{eq:lorentz} angegeben, an jeden der zuvor bestimmten Bereiche zu fitten.

\begin{equation}
   f(x)=a*\frac{\Gamma}{\Gamma^2 + {\left(x - x_0\right)}^2} + b
   \label{eq:lorentz}
\end{equation}

Der Parameter $a$ ist die Skalierung, $b$ ist der offset von der x-Achse und $\Gamma$ ist das ``full width half maximum'', also die Breite der Kurve bei der Hälfte des Maximums.
Interessant ist aber nur der Lageparameter $x_0$, der die Position entlang der x-Achse bestimmt.

Es war notwendig die Startparameter für die Berechnung anzugeben, damit der Fit konvergiert.
Tabelle~\ref{tab:start-param} listet die verwendeten Startparameter auf.
Speziell der Wert für $a$ ist notwendig, da die Kurven in den Spektren im Vergleich zu einer normierten Lorentz-Kurve um die x-Achse gespielt sind.
In Abbildung~\ref{fig:fit} sind die beiden berechneten Lorentz-Kurven mit Orange und Grün eingezeichnet.

\begin{figure}
   \centering
   \includegraphics[width=\hsize,clip,trim = 0 0 0 3350]{A2_lorentz_fit.png}
      \caption{Dasselbe Spektrum von Abb.~\ref{fig:spektrum}. Die Bereiche für die Lorentz-Fits sind mit Schwaz markiert, der Referenzwert mit Rot. Grün und Orange sind jeweils die berechneten Lorentz-Kurven.}
      \label{fig:fit}
\end{figure}

\begin{table}
   \caption{Startparameter für \emph{scipy.optimize.curve\_fit}. [min] bezeichnet das Minimum des Fit-Bereichs.}             % title of Table
   \label{tab:start-param}      % is used to refer this table in the text
   \centering                          % used for centering table
   \begin{tabular}{c c c c}        % centered columns (4 columns)
   \hline\hline                 % inserts double horizontal lines
   $x_0$ & $\Gamma$ & $a$ & $b$ \\    % table heading 
   \hline                        % inserts single horizontal line
      [min] & 1 & -1 & 1 \\      % inserting body of the table
   \hline                                   %inserts single line
   \end{tabular}
\end{table}

Für die Lokalisierung des Referenzwertes wurde einfach das Maximum des sichtbaren Peaks verwendet.
Da sich dieser Peak allerdings in manchen Spektren schon deutlich innerhalb des Bereichs der Na D Linien befindet, ist es nicht möglich einfach das Maximum des gesamten Spektrums zu nehmen.
Stattdessen wurde ein $\pm5$px Bereich um den geschätzten Wert 457px isoliert und davon das Maximum bestimmt.
Dies erlaubt es Verschiebungen des Referenzwertes auf der Pixelskala zu berücksichtigen.
Die einzige Ausnahme bei diesem Verfahren war das 8. Spektrum der Zeitserie.
Hier ist das Maximum des Referenzwertes sogar in einem 10px Intervall nicht eindeutig und es wurde einfach der Mittelwert 457px verwendet.
Tabelle~\ref{tab:fit-param} listet die Pixelwerte aller bestimmten Lageparameter auf.

\begin{table}
   \caption{Lageparameter als Ergebnis der Fits für die Na D Linien und den Referenzwert}             % title of Table
   \label{tab:fit-param}      % is used to refer this table in the text
   \centering                          % used for centering table
   \begin{tabular}{c c c c}        % centered columns (4 columns)
   \hline\hline                 % inserts double horizontal lines
   \# & $x_0$ Na (links) & $x_0$ Na (rechts) & Referenz \\    % table heading 
   \hline                        % inserts single horizontal line
   1  & 110.66 & 509.47 & 457 \\
   2  & 131.34 & 530.13 & 457 \\
   3  & 143.97 & 542.98 & 458 \\
   4  & 144.05 & 543.00 & 458 \\
   5  & 131.23 & 530.25 & 457 \\
   6  & 110.48 & 509.55 & 458 \\
   7  & 89.79  & 488.87 & 456 \\
   8  & 76.96  & 476.22 & 457 \\
   9  & 77.11  & 476.25 & 457 \\
   10 & 89.86  & 488.86 & 456 \\
   11 & 110.49 & 509.65 & 458 \\
   \hline                                   %inserts single line
   \end{tabular}
\end{table}


\section{Systemparameter des Doppelsternsystems}\label{sec:param-fit}

\begin{figure}
   \centering
   \includegraphics[width=\hsize,clip]{A2_sine.png}
      \caption{Aus der Verschiebung der Na D Linien berechnete Relativgeschwindigkeit aufgetragen über der Zeitskala der Spektren. Linke Na D linie in Blau, rechte in Orange.}
      \label{fig:sine}
\end{figure}

Im nächsten Schritt muss die Pixelskala auf abolute Wellenlängen umgerechnet werden, um $\Delta \lambda$ in Gleichung~\ref{eq:doppler} zu bestimmen.
Aus dem Fits-Header der Spektren ist abzulesen, dass die jeder Pixel einen Bereich von 0.015\AA\ auflöst.
Ausßerdem ist die Wellenlänge des Referenzwertes als 5895.04\AA\ gegeben.
Damit lässt sich die Pixelskala auf Wellenlängen umrechnen.

Aus Labormessungen ist bekannt, dass die Na D Linien bei 5889.95\AA\ und 5895.924\AA\ zu finden sind. Ihre Abstände zu dem Referenzwert können nun mit den Abständen der gemessenen Na D Linien zum Referenzwert verglichen werden. Die Differenz dieser Abstände ergibt dann die Dopplerverschiebung $\Delta \lambda$.

Im Fits-Header der Spektren ist ebenfalls vermerkt, dass das Zeitintervall $\Delta t$ der Zeitserie 1.54 Stunden beträgt.
Wird nun die mit Gleichung~\ref{eq:doppler} für jede der beiden Na Linien berechnete Relativgeschwindigkeit über der Zeitskala aufgetragen ergibt sich die in Abbildung~\ref{fig:sine} dargestellte Sinuskurve.

Aus dieser Kurve lassen sich mit einfachen Annahmen die Systemparameter des Doppelsterns abschätzen.
Die periodische Veränderung der Relativgeschwindigkeit kommt dadurch zustande, dass sich der vermessene Primärstern auf seiner Bahn einmal auf den Beobachter zu bewegt, und einmal von ihm weg.
Aus der Periode der Sinuskurve ergibt sich die Umlaufperiode des Doppelsterns.
Die gesamte Kurve ist außerdem auf der y-Achse verschoben, da sich das gesamte System mit einer Relativgeschwindigkeit gegenüber dem Beobachter bewegt.
Wird vom Betrag des Maximums (oder Minimums) der Kurve diese Nullpunktsverschiebung subtrahiert, erhält man die Amplitude der Kurve die gleich der linearen Bahngeschwindigkeit des Primärsterns ist.

Zur Bestimmung dieser Parameter wurde die Kurve durch folgende Funktion modelliert:
\begin{equation}\label{eq:sinus}
   \mathrm{V}(t) = V_0 + W\cos(2\pi\frac{t}{T}+b)
\end{equation}
Hierbei ist $V_0$ die Systemgeschwindigkeit, $W$ die lineare Bahngeschwindigkeit des Primärsterns ($=V_1$) und $T$ die Periode.
Die Phase $b$ dient zur Ausrichtung entlang der Zeitachse und hat keine weitere Bedeutung.
Die Parameter von Formel~\ref{eq:sinus} wurden mit einem erneuten Fit über \emph{scipy.optimize.curve\_fit} berechnet.
Der Residuenplot ist in Abbildung~\ref{fig:residues} dargestellt.
Es zeigt sich, dass keine Muster zu sehen sind und die Residuen alle recht nahe bei 0 sind, mit Werten von ca. $1/100$ der Amplitude.
Die Qualität der berechneten Parameter ist demnach als recht gut einzuschätzen.
Sie sind in Tabelle~\ref{tab:bahn-param} in SI-Einheiten gelistet.

\begin{figure}
   \centering
   \includegraphics[width=\hsize,clip]{A2_residues.png}
   \caption{Residuenplot des Fits für die Systemparameter nach Formel~\ref{eq:sinus}. X-Achse: $\Delta t$ in Stunden, y-Achse: V-Residuum in Einheiten von c.}
   \label{fig:residues}
\end{figure}

Für die Berechnung der weitern Systemparameter des Doppelsterns, insbesondere der Parameter des Sekundärsterns, sind weitere Annahmen und Formeln aus der Himmelsmechanik notwendig.
Als erste Vereinfachung wurde angenommen, dass sich die Sterne auf Kreisbahnen um den gemeinsamen Massenschwerpunkt bewegen.
Somit lässt sich der Bahnradius des Primärsterns über folgende Relation berechnen:
\begin{equation}\label{eq:r1}
   R_1 = \frac{T V_1}{2\pi}
\end{equation}
Des weiteren lässt sich aus der Definition des Massenschwerpunkts ein Ausdruck für $R_2$ Herleiten:
\begin{subequations}
   \begin{gather}
   \label{eq:a}
   a=R_1 + R_2 \mathrm{,\quad \dots\ Abstand\ der\ Sterne\ zueinander,} \\
   \label{eq:mrmr}
   M_1 R_1 = M_2 R_2 \mathrm{,\quad\ Einsetzen\ für\ } R_1 \mathrm{\ in~\ref{eq:a}:} \\
   \label{eq:r2}
   R_2 = a \frac{M_1}{M_1 + M_2}
   \end{gather}
\end{subequations}
Der Abstand $a$ kann über das 3. Keplersche Gesetzt (Gl.~\ref{eq:k3}) substituiert werden.
$R_2$ ergibt sich dann zu:
\begin{gather}
   \label{eq:k3}
   M_1 + M_2 = \frac{4\pi^2}{G} \frac{a^3}{T^3} \\
   \label{eq:r2-k3}
   R_2 = \sqrt[\leftroot{-2}\uproot{2}3]{\frac{M_1^3 T^2 G}{{(M_1 + M_2)}^2 4\pi^2}} 
\end{gather}
Dieser Ausdruck für $R_2$ (Gl.~\ref{eq:r2-k3}) ist nur noch von den Massen der Sterne und der zuvor berechneten Periode abhängig.
Ohne weitere Abschätzungen lässt sich $R_2$ trotzdem nicht berechnen, da die Massen unbekannt sind.

Ein guter Schätzwert für die Masse des Primärsterns ist der Mittelwert seines Spektraltyps F0V mit $\sim 1.2\mathrm{M}_\sun$.
Wird zusätzlich angenommen, dass $M_1 >> M_2$, der Primärstern also in Masse dominiert, kann in Gleichung~\ref{eq:r2-k3} der Beitrag von $M_2$ vernachlässigt werden und es ergibt sich:
\begin{equation}
   \label{eq:r2-ca}
   R_2 = \sqrt[\leftroot{-2}\uproot{2}3]{\frac{M_1 T^2 G}{4\pi^2}}
\end{equation}
Ob diese Annahme gerechtfertigt ist wird in Abschnitt~\ref{sec:dis} besprochen.

Nachdem nun $R_2$ berechnet werden kann, ist es möglich über Gleichung~\ref{eq:mrmr} auf $M_2$ zu kommen.
Genauso kann mit der Version für den Sekundärstern (Index 2) der Gleichung~\ref{eq:r1} die Bahngeschwindigkeit des Sekundärsterns berechnet werden.
In Tabelle~\ref{tab:bahn-param} sind jeweils die Mittelwerte von den für beide Linien nach der eben beschriebenen Methode berechneten Parametern in SI Einheiten angegeben.

\begin{table}
   \caption{Systemparameter des Doppelsternsystems. Gemittelt über die Ergebnisse beider Na D Linien.}
   \label{tab:bahn-param}
   \centering
   \begin{tabular}{l c c}
   \hline\hline
   \ & Primärstern & Sekundärstern \\
   \hline
   System V$_0$ [m/s] & \multicolumn{2}{c}{4781.63} \\
   Periode T [s] & \multicolumn{2}{c}{55591.53} \\
   Masse [kg] & $2.39\times10^{30}$ & $2.4\times10^{29}$ \\
   $M_1 / M_2$ & \multicolumn{2}{c}{$\sim 10$} \\
   Bahn-V$_i$ [m/s] & 26336.69 & 262072.02 \\
   Bahn-R$_i$ [m] & 233018228.79 & 2318725922.12 \\
   Spektraltyp & F0V & K/M \\
   \hline
   \end{tabular}
\end{table}


\section{Diskussion}\label{sec:dis}

Es zeigt sich, dass das berechnete Verhältnis der Massen der Sterne $M_1 / M_2$ etwa 10 ist.
Dieses Ergebnis steht nicht im direkten Wiederspruch zu der Annahme $M_1 >> M_2$, die auf Gleichung~\ref{eq:r2-ca} geführt hat.
Allerdings ist ein Faktor von 10 nur in groben Schätzungen zu vernachlässigen.
Wird die berechnete Masse des Sekundärsterns mit der typischen Masse seines Spektraltyps verglichen (für K/M liegt diese bei $\sim 0.5\mathrm{M}_\sun$), ist das berechnete Ergebnis wieder einer groben Schätzung näher als einem Wiederspruch.

Ein weiterer Aspekt, der bei den präsentierten Rechnungen nicht berücksichtigt wurde, ist eine mögliche Neigung der Rotationsebene des Doppelsternsystems gegenüber der Beobachtungsrichtung.
Die Ergebnisse sind streng genommen nur dann korrekt, wenn die Rotationsebene mit der Sichtlinie zusammenfällt.
Weist sie allerdings eine Neigung (Inklination $i$) auf, so sind die Ergebnisse nur eine untere Schranke für die wahren Werte.
Die beobachtete lineare Radialgeschwindigkeit ist dann um den Faktor $\sin(i)$ vermindert, der sich aus der Projektion der Rotationsebene auf die Sichtlinie ergibt.\\

Eine weitere interessante Frage ist, ob es mit der beschriebenen Methode möglich wäre Exoplaneten um ein Zentralgestirn nachzuweisen.
Der limitierende Faktor wäre hier das Auflösungsvermögen des Spektrografen, denn ein Planet hätte eine viel geringere Masse als der Sekundärstern.
Wird zum Beispiel ein Gasriese mit der Masse des Jupiters an die Stelle des Sekundärstern im betrachteten Doppelsternsystem gesetzt, kann eine Grenze für das Auflösungsvermögen berechnet werden.
Durch die veränderte Masse $M_2$ ergibt sich nach Gleichung~\ref{eq:mrmr} ein neuer Radius $R_1$ und somit ein neuer Abstand der Körper $a$.
Durch entsprechendes Umformen des 3. Keplerschen Gesetzes (Gl.~\ref{eq:k3}) kann dann die neue Periode T berechnet werden.
Aus T und $R_1$ ergibt sich nach Gleichung~\ref{eq:r1} eine neue Geschwindigkeit und somit eine neue Dopplerverschiebung nach Gleichung~\ref{eq:doppler}.
Diese neuen Werte sind in Tabelle~\ref{tab:ju} gelistet.

\begin{table}
   \caption{Systemparameter mit einem hypothetischen Gasriesen anstelle des Sekundärsterns.}
   \label{tab:ju}
   \centering
   \begin{tabular}{l c}
      \hline\hline
      T [s] & 55635.75 \\
      $V_1$ [m/s] & 208.3 \\
      $R_1$ [m] & 1844423.41 \\
      $\Delta\lambda$ [$\AA$] & 0.004 \\
      \hline
   \end{tabular}
\end{table}

Die Instrumente müssten also in der Lage sein, eine Dopplerverschiebung von $4\times {10}^{-3}\AA$ zu messen.
Das entspricht einem $\sim$ 4 mal größeren Auflösungsvermögen als die untersuchten Daten aufweisen.
Ohne bessere Instrumente wäre es also nicht möglich eine Dopplerverschiebung des Sterns aufgrund der Anwesenheit eines Gasriesen zu messen.

\section{Konklusion}\label{sec:conclusions}\label{sec:con}

\begin{description}
   \item[Methode:]
   Im Spektrum eines Doppelsternsystems wurde über die Dopplerverschiebung des Na D dupletts die lineare Radialgeschwindigkeit des Primärsterns bestimmt.
   Dazu wurden die Na D Linien durch eine Lorentz-Kurve parametrisiert und die Parameter an die Beobachtungsdaten gefittet.
   Die periodische Verschiebung der Linien ist zurückzuführen auf eine periodische Veränderung der Radialgeschwindigkeit.
   Die Periode, Amplitude und Nullpunktsverschiebung der resultierenden Sinuskurve wurden mit einem weiteren Fit berechnet.
   Anschließend wurden grundlegende Konzepte der Himmelsmechanik angewendet um aus den berechneten Systemparametern die noch übrigen Parameter zu bestimmen.
   Dabei wurde angenommen, dass die Masse des Primärsterns dominiert und einen für den Spektraltyp F0V typischen Wert hat.
   \item[Ergebnisse:]
   Die Ergebnisse sind in Tabelle~\ref{tab:bahn-param} gelistet.
   Die resultierenden Parameter stehen nicht im Wiederspruch zu den vereinfachenden Annahmen die getroffen wurden.
   Der Spektraltyp des Sekundärsterns kann mit guter Schätzung als K oder M angegeben werden.
   Allerdings stellen die berechneten Parameter nur eine untere Schranke der wahren Werte dar, da eine mögliche Inklination $i$ der Rotationsebene des Doppelsternsystems nicht berücksichtigt wurde.
   Die hier besprochene Methode kann nur dann zum Nachweis von Gasriesen in nahen Orbits um den Zentralstern verwendet werden, wenn das Auflösungsvermögen der Instrumente um den Faktor 4 erhöht wird.
\end{description}

\nocite{*}
\nobibliography{refs}
\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Examples for figures using graphicx

%-------------------------------------------------------------
%                 A figure as large as the width of the column
%-------------------------------------------------------------
   \begin{figure}
   \centering
   \includegraphics[width=\hsize]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                                    One column rotated figure
%-------------------------------------------------------------
   \begin{figure}
   \centering
   \includegraphics[angle=-90,width=3cm]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                        Figure with caption on the right side 
%-------------------------------------------------------------
   \begin{figure}
   \sidecaption
   \includegraphics[width=3cm]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                                Figure with a new BoundingBox 
%-------------------------------------------------------------
   \begin{figure}
   \centering
   \includegraphics[bb=10 20 100 300,width=3cm,clip]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                                      The "resizebox" command 
%-------------------------------------------------------------
   \begin{figure}
   \resizebox{\hsize}{!}
            {\includegraphics[bb=10 20 100 300,clip]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                                             Two column Figure 
%-------------------------------------------------------------
   \begin{figure*}
   \resizebox{\hsize}{!}
            {\includegraphics[bb=10 20 100 300,clip]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure*}
%
%-------------------------------------------------------------
%                                             Simple A&A Table
%-------------------------------------------------------------
%
\begin{table}
\caption{Nonlinear Model Results}             % title of Table
\label{table:1}      % is used to refer this table in the text
\centering                          % used for centering table
\begin{tabular}{c c c c}        % centered columns (4 columns)
\hline\hline                 % inserts double horizontal lines
HJD & $E$ & Method\#2 & Method\#3 \\    % table heading 
\hline                        % inserts single horizontal line
   1 & 50 & $-837$ & 970 \\      % inserting body of the table
   2 & 47 & 877    & 230 \\
   3 & 31 & 25     & 415 \\
   4 & 35 & 144    & 2356 \\
   5 & 45 & 300    & 556 \\ 
\hline                                   %inserts single line
\end{tabular}
\end{table}
%
%-------------------------------------------------------------
%                                             Two column Table 
%-------------------------------------------------------------
%
\begin{table*}
\caption{Nonlinear Model Results}             
\label{table:1}      
\centering          
\begin{tabular}{c c c c l l l }     % 7 columns 
\hline\hline       
                      % To combine 4 columns into a single one 
HJD & $E$ & Method\#2 & \multicolumn{4}{c}{Method\#3}\\ 
\hline                    
   1 & 50 & $-837$ & 970 & 65 & 67 & 78\\  
   2 & 47 & 877    & 230 & 567& 55 & 78\\
   3 & 31 & 25     & 415 & 567& 55 & 78\\
   4 & 35 & 144    & 2356& 567& 55 & 78 \\
   5 & 45 & 300    & 556 & 567& 55 & 78\\
\hline                  
\end{tabular}
\end{table*}
%
%-------------------------------------------------------------
%                                          Table with notes 
%-------------------------------------------------------------
%
% A single note
\begin{table}
\caption{\label{t7}Spectral types and photometry for stars in the
  region.}
\centering
\begin{tabular}{lccc}
\hline\hline
Star&Spectral type&RA(J2000)&Dec(J2000)\\
\hline
69           &B1\,V     &09 15 54.046 & $-$50 00 26.67\\
49           &B0.7\,V   &*09 15 54.570& $-$50 00 03.90\\
LS~1267~(86) &O8\,V     &09 15 52.787&11.07\\
24.6         &7.58      &1.37 &0.20\\
\hline
LS~1262      &B0\,V     &09 15 05.17&11.17\\
MO 2-119     &B0.5\,V   &09 15 33.7 &11.74\\
LS~1269      &O8.5\,V   &09 15 56.60&10.85\\
\hline
\end{tabular}
\tablefoot{The top panel shows likely members of Pismis~11. The second
panel contains likely members of Alicante~5. The bottom panel
displays stars outside the clusters.}
\end{table}
%
% More notes
%
\begin{table}
\caption{\label{t7}Spectral types and photometry for stars in the
  region.}
\centering
\begin{tabular}{lccc}
\hline\hline
Star&Spectral type&RA(J2000)&Dec(J2000)\\
\hline
69           &B1\,V     &09 15 54.046 & $-$50 00 26.67\\
49           &B0.7\,V   &*09 15 54.570& $-$50 00 03.90\\
LS~1267~(86) &O8\,V     &09 15 52.787&11.07\tablefootmark{a}\\
24.6         &7.58\tablefootmark{1}&1.37\tablefootmark{a}   &0.20\tablefootmark{a}\\
\hline
LS~1262      &B0\,V     &09 15 05.17&11.17\tablefootmark{b}\\
MO 2-119     &B0.5\,V   &09 15 33.7 &11.74\tablefootmark{c}\\
LS~1269      &O8.5\,V   &09 15 56.60&10.85\tablefootmark{d}\\
\hline
\end{tabular}
\tablefoot{The top panel shows likely members of Pismis~11. The second
panel contains likely members of Alicante~5. The bottom panel
displays stars outside the clusters.\\
\tablefoottext{a}{Photometry for MF13, LS~1267 and HD~80077 from
Dupont et al.}
\tablefoottext{b}{Photometry for LS~1262, LS~1269 from
Durand et al.}
\tablefoottext{c}{Photometry for MO2-119 from
Mathieu et al.}
}
\end{table}
%
%-------------------------------------------------------------
%                                       Table with references 
%-------------------------------------------------------------
%
\begin{table*}[h]
 \caption[]{\label{nearbylistaa2}List of nearby SNe used in this work.}
\begin{tabular}{lccc}
 \hline \hline
  SN name &
  Epoch &
 Bands &
  References \\
 &
  (with respect to $B$ maximum) &
 &
 \\ \hline
1981B   & 0 & {\it UBV} & 1\\
1986G   &  $-$3, $-$1, 0, 1, 2 & {\it BV}  & 2\\
1989B   & $-$5, $-$1, 0, 3, 5 & {\it UBVRI}  & 3, 4\\
1990N   & 2, 7 & {\it UBVRI}  & 5\\
1991M   & 3 & {\it VRI}  & 6\\
\hline
\noalign{\smallskip}
\multicolumn{4}{c}{ SNe 91bg-like} \\
\noalign{\smallskip}
\hline
1991bg   & 1, 2 & {\it BVRI}  & 7\\
1999by   & $-$5, $-$4, $-$3, 3, 4, 5 & {\it UBVRI}  & 8\\
\hline
\noalign{\smallskip}
\multicolumn{4}{c}{ SNe 91T-like} \\
\noalign{\smallskip}
\hline
1991T   & $-$3, 0 & {\it UBVRI}  &  9, 10\\
2000cx  & $-$3, $-$2, 0, 1, 5 & {\it UBVRI}  & 11\\ %
\hline
\end{tabular}
\tablebib{(1)~\citet{branch83};
(2) \citet{phillips87}; (3) \citet{barbon90}; (4) \citet{wells94};
(5) \citet{mazzali93}; (6) \citet{gomez98}; (7) \citet{kirshner93};
(8) \citet{patat96}; (9) \citet{salvo01}; (10) \citet{branch03};
(11) \citet{jha99}.
}
\end{table}
%-------------------------------------------------------------
%                      A rotated Two column Table in landscape  
%-------------------------------------------------------------
\begin{sidewaystable*}
\caption{Summary for ISOCAM sources with mid-IR excess 
(YSO candidates).}\label{YSOtable}
\centering
\begin{tabular}{crrlcl} 
\hline\hline             
ISO-L1551 & $F_{6.7}$~[mJy] & $\alpha_{6.7-14.3}$ 
& YSO type$^{d}$ & Status & Comments\\
\hline
  \multicolumn{6}{c}{\it New YSO candidates}\\ % To combine 6 columns into a single one
\hline
  1 & 1.56 $\pm$ 0.47 & --    & Class II$^{c}$ & New & Mid\\
  2 & 0.79:           & 0.97: & Class II ?     & New & \\
  3 & 4.95 $\pm$ 0.68 & 3.18  & Class II / III & New & \\
  5 & 1.44 $\pm$ 0.33 & 1.88  & Class II       & New & \\
\hline
  \multicolumn{6}{c}{\it Previously known YSOs} \\
\hline
  61 & 0.89 $\pm$ 0.58 & 1.77 & Class I & \object{HH 30} & Circumstellar disk\\
  96 & 38.34 $\pm$ 0.71 & 37.5& Class II& MHO 5          & Spectral type\\
\hline
\end{tabular}
\end{sidewaystable*}
%-------------------------------------------------------------
%                      A rotated One column Table in landscape  
%-------------------------------------------------------------
\begin{sidewaystable}
\caption{Summary for ISOCAM sources with mid-IR excess 
(YSO candidates).}\label{YSOtable}
\centering
\begin{tabular}{crrlcl} 
\hline\hline             
ISO-L1551 & $F_{6.7}$~[mJy] & $\alpha_{6.7-14.3}$ 
& YSO type$^{d}$ & Status & Comments\\
\hline
  \multicolumn{6}{c}{\it New YSO candidates}\\ % To combine 6 columns into a single one
\hline
  1 & 1.56 $\pm$ 0.47 & --    & Class II$^{c}$ & New & Mid\\
  2 & 0.79:           & 0.97: & Class II ?     & New & \\
  3 & 4.95 $\pm$ 0.68 & 3.18  & Class II / III & New & \\
  5 & 1.44 $\pm$ 0.33 & 1.88  & Class II       & New & \\
\hline
  \multicolumn{6}{c}{\it Previously known YSOs} \\
\hline
  61 & 0.89 $\pm$ 0.58 & 1.77 & Class I & \object{HH 30} & Circumstellar disk\\
  96 & 38.34 $\pm$ 0.71 & 37.5& Class II& MHO 5          & Spectral type\\
\hline
\end{tabular}
\end{sidewaystable}
%
%-------------------------------------------------------------
%                              Table longer than a single page  
%-------------------------------------------------------------
% All long tables will be placed automatically at the end of the document
%
\longtab{
\begin{longtable}{lllrrr}
\caption{\label{kstars} Sample stars with absolute magnitude}\\
\hline\hline
Catalogue& $M_{V}$ & Spectral & Distance & Mode & Count Rate \\
\hline
\endfirsthead
\caption{continued.}\\
\hline\hline
Catalogue& $M_{V}$ & Spectral & Distance & Mode & Count Rate \\
\hline
\endhead
\hline
\endfoot
%%
Gl 33    & 6.37 & K2 V & 7.46 & S & 0.043170\\
Gl 66AB  & 6.26 & K2 V & 8.15 & S & 0.260478\\
Gl 68    & 5.87 & K1 V & 7.47 & P & 0.026610\\
         &      &      &      & H & 0.008686\\
Gl 86 
\footnote{Source not included in the HRI catalog. See Sect.~5.4.2 for details.}
         & 5.92 & K0 V & 10.91& S & 0.058230\\
\end{longtable}
}
%
%-------------------------------------------------------------
%                              Table longer than a single page
%                                            and in landscape, 
%                    in the preamble, use: \usepackage{lscape}
%-------------------------------------------------------------

% All long tables will be placed automatically at the end of the document
%
\longtab{
\begin{landscape}
\begin{longtable}{lllrrr}
\caption{\label{kstars} Sample stars with absolute magnitude}\\
\hline\hline
Catalogue& $M_{V}$ & Spectral & Distance & Mode & Count Rate \\
\hline
\endfirsthead
\caption{continued.}\\
\hline\hline
Catalogue& $M_{V}$ & Spectral & Distance & Mode & Count Rate \\
\hline
\endhead
\hline
\endfoot
%%
Gl 33    & 6.37 & K2 V & 7.46 & S & 0.043170\\
Gl 66AB  & 6.26 & K2 V & 8.15 & S & 0.260478\\
Gl 68    & 5.87 & K1 V & 7.47 & P & 0.026610\\
         &      &      &      & H & 0.008686\\
Gl 86
\footnote{Source not included in the HRI catalog. See Sect.~5.4.2 for details.}
         & 5.92 & K0 V & 10.91& S & 0.058230\\
\end{longtable}
\end{landscape}
}
%
%-------------------------------------------------------------
%               Appendices have to be placed at the end, after
%                                        \end{thebibliography}
%-------------------------------------------------------------
\end{thebibliography}

\begin{appendix} %First appendix
\section{Background galaxy number counts and shear noise-levels}
Because the optical images used in this analysis...
\begin{figure*}%f1
\includegraphics[width=10.9cm]{1787f23.eps}
\caption{Shown in greyscale is a...}
\label{cl12301}
\end{figure*}

In this case....
\begin{figure*}
\centering
\includegraphics[width=16.4cm,clip]{1787f24.ps}
\caption{Plotted above...}
\label{appfig}
\end{figure*}

Because the optical images...

\section{Title of Second appendix.....} %Second appendix
These studies, however, have faced...
\begin{table}
\caption{Complexes characterisation.}\label{starbursts}
\centering
\begin{tabular}{lccc}
\hline \hline
Complex & $F_{60}$ & 8.6 &  No. of  \\
...
\hline
\end{tabular}
\end{table}

The second method produces...
\end{appendix}
%
%
\end{document}

%
%-------------------------------------------------------------
%          For the appendices, table longer than a single page
%-------------------------------------------------------------

% Table will be print automatically at the end of the document, 
% after the whole appendices

\begin{appendix} %First appendix
\section{Background galaxy number counts and shear noise-levels}

% In the appendices do not forget to put the counter of the table 
% as an option

\longtab[1]{
\begin{longtable}{lrcrrrrrrrrl}
\caption{Line data and abundances ...}\\
\hline
\hline
Def & mol & Ion & $\lambda$ & $\chi$ & $\log gf$ & N & e &  rad & $\delta$ & $\delta$ 
red & References \\
\hline
\endfirsthead
\caption{Continued.} \\
\hline
Def & mol & Ion & $\lambda$ & $\chi$ & $\log gf$ & B & C &  rad & $\delta$ & $\delta$ 
red & References \\
\hline
\endhead
\hline
\endfoot
\hline
\endlastfoot
A & CH & 1 &3638 & 0.002 & $-$2.551 &  &  &  & $-$150 & 150 &  Jorgensen et al. (1996) \\                    
\end{longtable}
}% End longtab
\end{appendix}

%-------------------------------------------------------------
%                   For appendices and landscape, large table:
%                    in the preamble, use: \usepackage{lscape}
%-------------------------------------------------------------

\begin{appendix} %First appendix
%
\longtab[1]{
\begin{landscape}
\begin{longtable}{lrcrrrrrrrrl}
...
\end{longtable}
\end{landscape}
}% End longtab
\end{appendix}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  list for correct label nameing scheme
%
% | ch: 	chapter
% | sec: 	section
% | subsec: 	subsection
% | fig: 	figure
% | tab: 	table
% | eq: 	equation
% | lst: 	code listing
% | itm: 	enumerated list item
% | alg: 	algorithm
% | app: 	appendix subsection
%
%
% what to abbreviate (unless at beginning of sentence):
%
% Sect., Sects., Fig., Figs., Col., Cols.
% Table is never abbreviated
%
%
% citations
%
% \citet{jon90}					⇒	Jones et al. (1990)
% \citep{jon90}					⇒	(Jones et al. 1990)
% \citep[see][]{jon90}				⇒	(see Jones et al. 1990)
% \citep[see][chap. ̃2]{jon90}		⇒	(see Jones et al. 1990, chap. 2)
%
% Multiple citations by including more than one citation key in the \cite command
% \citet{jon90,jam91}			⇒	Jones et al. (1990); James et al. (1991)
% \citep{jon90,jam91}			⇒	(Jones et al., 1990; James et al. 1991)
% \citep{jon90,jon91}			⇒	(Jones et al. 1990, 1991)
% \citep{jon90a,jon90b}		⇒	(Jones et al. 1990a,b)
%
