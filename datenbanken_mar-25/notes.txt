

aufgabe 1
    simbad screenshot: pleiades_simbad_data.png

    ICRS Koordinaten:                   Grad in dezimal
        RA(H M S.ss): 03 47 00.0          56.75  (H*360/24 + M*360/60/24 + S*360/60/60/24)
        DEC(G BM BS): +24 07 00           24.1166666... (G + BM/60 + BS/60/60)
        
    definition ICRS:
        http://hpiers.obspm.fr/icrs-pc/newwww/misc/icrs.php
        
    definition GAL:
        http://adsabs.harvard.edu/full/1960MNRAS.121..123B
        + citation from here: https://ui.adsabs.harvard.edu/abs/1960MNRAS.121..123B/abstract

    IDs        
        M 45    	    H 0346+24 	        OCl 421.0 	
        C 0344+239  	NAME Pleiades 	    [KPR2004b] 47 	
        Cl Melotte 22 	NAME Seven Sisters 	[KPS2012] MWSC 0305 
    
    bedeutung (durch klicken auf link):
        M: messier katalog
        C: cluster id mit RADEC als nummer
        Cl: by	MELOTTE P.J.
          	A catalogue of star clusters shown on the Franklin-Adams chart plates.
          	
        H: katalog der HEAO satellit mission
        OCL: Open (galactic) Cluster ; Catalogue of star clusters and associations
        KPR2004b: Kharchenko+Piskunov+Roesser+, 2004
        KPS2012: Kharchenko+Piskunov+Schilbach+, 2012
        
        for hyaden
        Cl Collinder:
        	COLLINDER P.
           	On structured properties of open galactic clusters and their spatial distribution.
        
    simbad link:
        http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Pleiades
        
    
aufgabe 2

    gaia query:
        SELECT ra,dec,pmra,pmdec,parallax,parallax_error,phot_g_mean_mag,phot_bp_mean_mag,phot_rp_mean_mag FROM gaiadr2.gaia_source
        WHERE CONTAINS(POINT('ICRS', gaiadr2.gaia_source.ra, gaiadr2.gaia_source.dec),CIRCLE('ICRS', 56.75, 24.11666, 5))=1 AND gaiadr2.gaia_source.parallax > 5
        
        im protokoll beschreiben

aufgabe 3
        
    Topcat:
    
        column screenshot: pleiades_gaia_result_columns.png
        einheit: mas => milli arcseconds
        spalten 10+11 sind von mir

      plots:  
        histogram fuer entfernung: pleiades_distance_hist.png
            quadratischer anstieg: oberflaeche der Kuegel mit radius der entfernung
            peak: dort wo sternhaufen ist
        histogram mit markiertem bereich fuer pleiades: pleiades_distance_hist_subset.png
        
        farbe vs helligkeit: pleiades_BP-RP_v_abG.png
            ungewoenlich: waagrechte verteilung
            hauptbestandteil der betrachteten sterene sollten im sternhaufen liegen, also gleich entfernt => bilden schoene hauptreihe
        
        proper motion: pleiades_pm_plot.png
        proper motion zoomed on pleiades: pleiades_pm_plot_zoomed.png
        
      Entfernung: 137.168 pc / ~137 pc
      
      entfernung ueber photometrische daten:
            unterschied der scheinbaren helligkeiten: 2.625 mag
            Annahme: absolute Helligkeiten (M) der Sterne gleich (gleiche Population im bereich der messung des magnitudenunterschieds):
              (1)  Hyaden:     m1 - M = 5log(r1) -5
              (2)  Pleiaden:   m2 - M = 5log(r2) -5
                   Differenz:  m1 - m2 = 5log(r1/r2)   = -2.625
                   =>>         r1/r2 = 10^([m1-m2]/5)  = 0.3
      
      bonus:
      ueber die entfernung im histogram: pleiades_distance_hist_subset.png
      engeren bereich nehmen, sodass der "durchmesser" des haufens ein paar ~10 pc
        
      
aufgabe 4

    zoom auf pleiades zentrum: aladin_pleiades_dss2_color.png
  durchmesser:
    aladin_pleiades_size_measure.png
    10 deg fuer komplette auswahl
    mit einschraenken auf kompakte stelle: 3 deg
    umrechung auf parsec: deg => rad => dist*rad = durchmesser
        deg/360*2Pi * dist = 7.18pc
        
    bemerkungen:
        sinvoll fuer eine grobe schaeztung, ja
        keine genaue messung moeglich
        dazu muesste man die auswahl strenger machen...
        
aufgabe 5

    references:
        \ref{par} chapter 7 "THE DISTANCE TO THE PLEIADES":
            The Gaia DR2 release (Gaia Collaboration et al. 2018b) estimates the Pleiades’ distance at 135.8±0.1 pc. Applying our parallax offset to this estimate brings the distance moderately down to 134.8±0.2 pc, and back towards the averageof the stellar-physics-based estimates.
            
        \ref{gaia_data} is the reference for (Gaia Collaboration et al. 2018b)
        
        \ref{5dplei}
        We infer a distance of 135.15±0.43pc  for  the  Pleiades  from  the  Bayesian  approach  of  Luri  et  al.(2018)
        \ref{luri} is mentioned here
        

Aufteilung des Protokolls:
                Antonia     Kayran
    Aufgabe 1       x
    Aufgabe 2                  x
    Aufgabe 3                  x
    Aufgabe 4       x
    Aufgabe 5       x
