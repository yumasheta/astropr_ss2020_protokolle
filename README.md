# Astronomisches Praktikum SS2020

[![CI_LaTeX_Compile_Test](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/badges/master/pipeline.svg)](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/pipelines)

## PDF Downloads:

- Template: [here](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/raw/template/template.pdf?job=compile_LaTeX)

- LaTeX 18. Maerz: [here](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/raw/latex_mar-18-kayran/latex_mar-18-kayran.pdf?job=compile_LaTeX)  
  Autor: Kayran Schmidt

- Datenbanken 25. Maerz: [here](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/raw/datenbanken_mar-25/datenbanken_mar-25.pdf?job=compile_LaTeX)  
  Autoren: Antonia Magenheim, Kayran Schmidt

- Spektroskopie 22. April:  
  Autoren: Antonia Magenheim, Kayran Schmidt
  - Beispiel 2: [here](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/raw/spektroskopie_apr-22/spektroskopie_apr-22_A2.pdf?job=compile_LaTeX)
  - Beispiel 3: [here](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/raw/spektroskopie_apr-22/spektroskopie_apr-22_A3.pdf?job=compile_LaTeX)
<br>

- UK-Schmidt 29. April: [here](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/raw/uk-schmidt_apr-29/uk-schmidt_apr-29.pdf?job=compile_LaTeX)  
  Autoren: Antonia Magenheim, Kayran Schmidt

- Radioastronomie 3. Juni: [here](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/raw/radio_jun-3/radio_jun-3.pdf?job=compile_LaTeX)  
  Autoren: Antonia Magenheim, Kayran Schmidt

- Geometrische Optik 17. Juni: [here](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/raw/geom-op_jun-17/geom-op_jun-17.pdf?job=compile_LaTeX)  
  Autoren: Antonia Magenheim, Kayran Schmidt

### Alle PDFs im .zip Archiv:
- [link](https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/download?job=compile_LaTeX)

#### Template für Download Links:
`https://gitlab.com/yumasheta/astropr_ss2020_protokolle/-/jobs/artifacts/master/raw/<folder>/<file.pdf>?job=compile_LaTeX`

`<folder>`: Pfad relativ zu Repository  
`<file.pdf>`: Name des PDF

* * * *

#### Regeln für neue Ordner

Das .tex LaTeX file muss 1 level unter dem Repository Ordner sein.  
Bsp:
```
AstroPR_SS2020_Protokolle/
│   ...
├── semAktForsch            <= OK
│   ├── semaktForsch.bib
│   └── semaktForsch.tex
└── test-template
    └── neuerOrdner         <= nicht OK
        ├── refs.bib
        └── test-template.tex
```
