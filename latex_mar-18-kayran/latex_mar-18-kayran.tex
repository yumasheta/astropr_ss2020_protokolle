% options for aa style
%
%\documentclass[referee]{aa} % for a referee version
%\documentclass[onecolumn]{aa} % for a paper on 1 column  
%\documentclass[longauth]{aa} % for the long lists of affiliations 
%\documentclass[letter]{aa} % for the letters 
%\documentclass[bibyear]{aa} % if the references are not structured 
%                              according to the author-year natbib style

\documentclass[utf8]{../aa-package/aa}

\usepackage{graphicx}
\usepackage[varg]{txfonts}

%\usepackage[options]{hyperref}
% To add links in your PDF file, use the package "hyperref"
% with options according to your LaTeX or PDFLaTeX drivers.
%
% hidelinks removes red border from clickable links
\usepackage[hidelinks]{hyperref}

% taken from the lecture slides, ''lebensnotwendig'':
% the rest is already in the aa.cls style definition
\usepackage{lmodern}

% use external bib file with aa citation style
\usepackage{natbib}
\bibpunct{(}{)}{;}{a}{}{,} % to follow the A&A style
\bibliographystyle{../aa-package/bibtex/aa}

% add captions to subfigures
\usepackage{subcaption}

% horizontal (in text) enumerations
\usepackage[inline]{enumitem}

% generate some text
\usepackage[math]{blindtext}
\usepackage{lipsum}

% hyphenation rules for custom words
\hyphenation{FITS}
\hyphenation{PACS}
\hyphenation{HPPJSMAP}
\hyphenation{HPPUNIMAP}


\begin{document} 

% set the language
% [english , ngerman], default is english
% \selectlanguage{ngerman}


   \title{\LaTeX\ Laboratory Exercises}

   \subtitle{Visualizing FITS Files and using Python for Gaussian Fits}

   \author{
	Kayran Schmidt (Mat.Nr.: 01604789)
          %\inst{\ref{kayran-inst}}
          }

   \institute{
   	     Institute for Astronomy (IfA), University of Vienna, Bachelor of Science Program 033 661, \\
   	     Course 280445 [PR PM-AstroPR] Astronomical Laboratory Exercises for Beginners \\
              \email{\href{mailto:a01604789@unet.univie.ac.at}{a01604789@unet.univie.ac.at}}\label{kayran-inst}
             }

   \date{March 18, 2020}

% \abstract{}{}{}{}{} 
% 5 {} token are mandatory

% it is possible to leave {} empty if optional 
  \abstract
  % context heading (optional)
   {Exercises for the Astronomical Laboratory Course (280445).}
  % aims heading (mandatory)
   {Working with FITS files from Herschel observations and exploring different visualization possibilities as well as fitting Gaussians to measurement data to extract information about the underlying electronics.}
  % methods heading (mandatory)
   {The FITS files are examined with the application DS9. Using its features an image combining red and blue channels is created. The necessary context with a coordinate grid and a scale measure are also provided with the image. The fitting algorithms used are primarily the routines provided by the stats module of the scipy package. Additionally the final fit is made by calculating the expected value and standard deviation of the data, which are the two defining parameters of a Gaussian.}
  % results heading (mandatory)
   {The dual channel image shows interesting features of dust clouds around the center star. The final Gaussian fit allows to specify the voltage bias and noise introduced by the measurement electronics.}
  % conclusions heading (optional), leave it empty if necessary 
   {}

 % standard abstract with no structured sections takes only one argument
 %\abstract{\lipsum[1]}

% seperate by '--'
   \keywords{FITS Files -- DS9 -- Alpha ORI -- Python -- Gauss Fits -- CCD Noise}

   \maketitle
%
%-------------------------------------------------------------------

\section{Introduction}\label{sec:intro}

The following exercises provide an in depth look into the challenges professional astronomers face on a daily basis.
\par
Methods for visualizing and working with images taken by the Herschel space telescope will be presented.
The images are encoded with the FITS file format, which is common for astronomical use cases.
\par
In addition, fitting Gaussians using Python will be explored.
The parameters for the fits represent properties of the measurement electronics.

% make sure the results of exercise 1 stay together, the pointe is at the last sentence


\section{Exercise 1}

The goal of the first exercise is to work with FITS files using DS9%
\footnote{SAOImage DS9 (short ``DS9'') is an astronomical imaging and data visualization application. \\See: \mbox{\url{http://ds9.si.edu/site/Home.html}}} %
and to create a ``good looking'' example picture.
Image data for Alpha ORI (commonly known as ``Betelgeuse'') from the Herschel Science Archive were provided to work with.


\subsection{Image Details}
\label{subsec:img-det}

\begin{table*}
	\caption{Selected Parts (in order) of the FITS Header of the Image used for the Blue Channel of Figure \ref{fig:rgb-image}}
	\label{tab:fits-header}
	\centering
	\begin{tabular}{c c l}
		\hline\hline
		Key-Name & Value & Comment\\
		\hline
		TYPE & `HPPUNIMAPB' & Product Type Identification \\
		DATE & `2016-07-24T04:39:44.536000' & Creation date of this product \\
		INSTRUME & `PACS' & Instrument attached to this product \\
		WAVELNTH & 70.0 & [micrometer] Filter reference wavelength \\
		LEVEL & '25' & Product level \\
		ORIGIN & `Herschel Science Centre' & Site that created the product \\
		OBJECT & 'ALPHA ORI' &  Target name \\
		TELESCOP & 'Herschel Space Observatory' & Name of telescope \\
		\hline
	\end{tabular}
\end{table*}

The images were produced by an observation with the PACS (Photodetector Array Camera and Spectrometer) instrument on board the Herschel Space Telescope.
The observation is dated for July 24, 2016.
The images associated with each observation are grouped according to their processing level.
The possible levels are:
\begin{enumerate*}[label=(\alph*)]
\item Level 0\label{list:level-0}
\item Level 0.5
\item Level 1
\item Level 2\label{list:level-2}
\item Level 2.5\label{list:level-25}
\item Level 3
\end{enumerate*}.
The differences between these levels are the processing steps performed on the raw data cube \ref{list:level-0}.
For detailed information on the processes refer to chapter 10.3 of the PACS Handbook \citep{herschel-pacs-hb}.\\
For each level different FITS files from different mapping methods are available.
The ``high-level pipeline products'' \citep[see][chap. 10.3.2]{herschel-pacs-hb} provide the images with the most amount of processing.
They are only available from \ref{list:level-2} onward.
\par
For this exercise images from different mapping methods at \ref{list:level-25} were used.
A list of all possible mapping methods and their products can be found in chapter 4.1.6 of the Herschel Product Definition Document \citep{herschel-product-def}.\\
For the process of choosing which mapping method to use for the final picture, the images from the methods HPPJSMAP \citep[see][chap. 10.3.2.1.2]{herschel-pacs-hb} and  HPPUNIMAP \citep[see][chap. 10.3.2.1.3]{herschel-pacs-hb} were compared.
Both methods provide an image taken at $70\mathrm{\mu m}$ (blue) and $160\mathrm{\mu m}$ (red).
The final decision is based on the image in the blue band, because it has a higher resolution.
Ultimately, it has been determined, that the HPPUNIMAP method provides the best pictures, because of its smoothened nature.
In contrast, the HPPJSMAP method produced a more pixelated picture.
For reference, figure \ref{fig:bw-comp} in appendix \ref{app:images} contains the images used for the mentioned comparison.
\par
Table \ref{tab:fits-header} lists the key properties extracted from the FITS header referenced in this section for the image that was used as the blue channel of figure \ref{fig:rgb-image}.


\subsection{Image Editing}
\label{subsec:img-edit}

The two images used for the final picture (fig. \ref{fig:rgb-image}) are the red and blue images of processing level 2.5 with the HPPUNIMAP mapping method.
In order to combine the two images into one picture the ``RGB Channel'' feature\footnote{That feature was not part of the preparation class for this exercise. Thus the following official guide was consulted: \mbox{\url{http://ds9.si.edu/doc/user/rgb/index.html}}} of DS9 was used.
Each image was loaded into its corresponding channel and its cut values were adjusted to the liking of the author.
\par
The values finally settled on are listed in table \ref{tab:cut-vals} and were used on a logarithmic scale.
If the ``Low'' value is too low, features of the dust clouds are suppressed by noise.
If it is too high, the dust features become indistinguishable from the black background.
Similarly, when the ``High'' value is too high, the dust clouds are suppressed by the bright star in the center of the image.
If it is too low, the dust features loose detail.
\par
Using DS9's ``Coordinate Grid'' feature, the image was embedded into its reference frame.
Additionally, a scale with $500''$ in length is drawn next to the center of the picture to provide orientation for the dimensions of the structural features.
The coordinates of the endpoints of this scale are listed in table \ref{tab:scale}.
\par
The final picture, as it is presented in figure \ref{fig:rgb-image}, is however, not reproducible by only using DS9.
Due to a limitation of the software, it is not possible to display the values for the color ramp on the bottom of the image, for both the red and blue channels.
Only one channel's values can be displayed at a time.
They will show up below the color ramp.
In order to have the values for both channels displayed, a copy of the red channel's values was placed above the color ramp using additional image editing software.

\begin{table}
	\caption{Cut Limits for the Red (R) and Blue (B) Images}
	\label{tab:cut-vals}
	\centering
	\begin{tabular}{c c c}
		\hline\hline
		Image & Low & High \\
		\hline
		R & $-0.00649143$ & $0.00756539$ \\
		B & $-0.00193495$ & $0.00299807$ \\
		\hline
	\end{tabular}
\end{table}

\begin{table}
	\caption{Coordinates of the Scale Endpoints}
	\label{tab:scale}
	\centering
	\begin{tabular}{c c c}
		\hline\hline
		\ & RA & DEC \\
		\hline
		Point 1 & $5:54:39.0$ & $+7:20:00.0$ \\
		Point 2 & $5:54:39.0$ & $+7:28:20.0$ \\
		\hline
	\end{tabular}
\end{table}


\subsection{Results}

\begin{figure}
	\centering
	\includegraphics[width=\hsize]{HPPUNIMAPB_RGB}
	\caption{Image for combined red and blue channels colored respectively. The image for the red channel was taken at a wavelength of $160\mathrm{\mu m}$, the image for the blue channel at a wavelength of $70\mathrm{\mu m}$. With figure \ref{fig:big-rgb} in appendix \ref{app:images} a larger version of this picture is provided.}
	\label{fig:rgb-image}
\end{figure}

The final image (fig. \ref{fig:rgb-image}) allows for speculations about the origins and properties of the depicted structures.
Arguably, the circular features around the center star could be the response of dust clouds to the emissions coming from that star.
The red spots are cooler parts of the dust, emitting thermal radiation and thus appearing in the red band.\\
However, the straight line in the upper left has to originate from man.
It perfectly matches the trail that starship ``Enterprise'' leaves behind when traveling with warp speed.

\newpage
\section{Exercise 2}

The goal of the second exercise was to analyze a CCD recording of a stray light gradient taken with Cheops.
Every CCD measurement is taken with a positive base voltage pre applied to prevent errors in the electronic processing and is affected by random noise.
Both, the voltage bias and noise can be extracted from the boundary regions of the CCD image.
The means used for the elaboration on hand is a Python script that is included as appendix \ref{app:code-sample}.
\par
As a first step the provided FITS file was loaded into a numpy array and shuffled to disguise any patterns in the data.
The blue line of figure \ref{fig:plot} represents the corresponding normed histogram, generated with the ``hist()'' function of the matplotlib\footnote{See: \url{https://matplotlib.org/index.html}} library.
The bin size was chosen to be the square root of the data size.
Upon inspection of the histogram, the boundary regions of the CCD image can be identified as the gauss shaped part of the distribution on the very left hand side.


\subsection{Fitting a Gaussian}

The formula used throughout for the Gaussian fits is as follows:
\begin{equation}
	\label{eq:gauss}
	f(x) = \frac{1}{\sqrt{2\pi\sigma^2}}\mathrm{e}^{-\frac{1}{2}\left( \frac{x - \mu}{\sigma}\right)^2},
\end{equation}
with the location parameter $\mu$ and the scale parameter $\sigma$ to be determined for each fit.
\par
The fitting algorithm used in the Python script (app. \ref{app:code-sample}) is part of the stats module of the scipy\footnote{See: \url{https://scipy.org/}\\\mbox{For the stats library see:} \\\mbox{\url{https://docs.scipy.org/doc/scipy/reference/stats.html}}} library.
The orange line in figure \ref{fig:plot} shows a gauss fit using that module on the complete data set.
However, the discussion of the reasonability of such a fit shall be left to fellow colleagues.
\newline\par
In order to achieve the main goal, extracting the voltage bias and noise parameters, fitting a Gaussian only to the part of the distribution identified earlier as the contribution of the boundary regions is necessary.
A first attempt would consist of using the same fitting method as before on a subset of the data.
The magenta line in figure \ref{fig:plot} shows the result of a fit restricted to values in the interval $[725, 775]$.
Rescaling the resulting probability density function (pdf) with respect to the complete data was necessary to be able to depict the graphs in the same plot.
The values for the parameters are listed in the third row of table \ref{tab:fit-params}.
\par
Iterating on that attempt leads to the desire to remove the contribution of the stray light gradient (rest of the distribution) to the segment in question.
A simple attempt would be to fit an exponential to the rising signal edge and subtract its values from the distribution's values in the considered interval.
\par
The desired pdf has the shape of equation \ref{eq:exp}, where $\xi$ absorbs any additional parameters.
\begin{equation}
	\label{eq:exp}
	f(\xi) =\left|\lambda\right|\mathrm{e}^{\lambda \xi}
\end{equation}
Unfortunately, the stats module only allows for normed exponential distributions on the interval $[\alpha, \infty) \text{~with:} \left|\alpha\right| < \infty$, which requires a negative parameter $\lambda$.
In order to overcome this limitation, the exponential fit is performed on the data multiplied with $-1$, so it would appear as mirrored on the y-axis.
Additionally, the region of the fit is constrained to the interval $(-\infty, 725] \cup [775, 800]$ which leaves the region of interest out and cuts off at a reasonable value before the signal edge flattens out.
\par
The resulting $\lambda$ parameter is listed in the second row of table \ref{tab:fit-params}.
The green line in figure \ref{fig:plot} shows the pdf according to equation \ref{eq:exp} and renormalized to the complete data.
\par
As a next step the pdf values of the exponential fit were subtracted from the histogram values in the interval $[725, 775]$.
In order to fit a Gaussian to the modified distribution a simpler method than the fit algorithm of the stats module is used.
Consulting common statistics knowledge, formulas \ref{eq:expct} and \ref{eq:std} are used to calculate the expected value and standard deviation of the modified data.
\begin{align}
	\text{Expected Value:~} \mathrm{E}(x) &= \sum\limits_{i=0}^{\mathrm{N}} x_i~pdf(x_i)\label{eq:expct}\\
	\text{Standard Deviation:~} \mathrm{Std}(x) &= \sqrt{\sum\limits_{i=0}^{\mathrm{N}} \left(x_i - E(x)\right)^2 pdf(x_i)} \label{eq:std}
\end{align}
Here N is the number of bins and the value of the pdf evaluated at the bin edges ($x_i$) is calculated as $\left(\text{\# in bin}/\text{total}\right)$ which should be considered equivalent for all intents and purposes of the elaboration on hand\footnote{Discussions surrounding the applicability of the Laplacian probability measure used here shall not be covered.}. 
\par
The values calculated with formulas \ref{eq:expct} and \ref{eq:std} are the location parameter $\mu$ and scale parameter $\sigma$ of the underlying Gaussian distribution.
They are listed in row 4 of table \ref{tab:fit-params}.
The red line in figure \ref{fig:plot} shows the corrected Gaussian fit.

\begin{figure*}
	\centering
	
	\resizebox{\hsize}{!}{\includegraphics[trim=105 45 118 0,clip]{plot}}
	\caption{Histogram and fitted curves of the stray light gradient measurement. The red line represents the final corrected Gaussian fit for the voltage bias and noise.}
	\label{fig:plot}
\end{figure*}

\begin{table}
	\caption{Fit Parameters}
	\label{tab:fit-params}
	\centering
	\begin{tabular}{l l l}
		\hline\hline
		Fit & \multicolumn{2}{c}{Parameters} \\
		\hline
		Overall Gauss & $\mu=1301.91$ & $\sigma=305.7$ \\
		Exp for Signal Edge & $\lambda=787.44$ & \ \\
		Noise Gauss & $\mu=750.42$ & $\sigma=10.32$ \\
		Noise Gauss corrected & $\mu=748.64$ & $\sigma=9.42$ \\
		\hline
	\end{tabular}
\end{table}


\subsection{Results}

Considering the origin of the analyzed data it is now possible to relate the parameters calculated for the fits with the voltage bias and noise of the CCD measurement.
The connection of the expected value and location parameter for a Gaussian has already been drawn.
From there, identifying that parameter as the voltage bias is the next logical step.
The standard deviation (scale parameter) of the distribution will act as a measure for the random noise introduced by the electronics.
\par
For the final result the previously discussed exponential fit for the rising edge of the stray light gradient was settled on.
That method should provide superior numbers compared to the simplest correction method of using a constant offset, which would equate to fitting a constant instead of the exponential.
However, colleagues may be challenged to finding better methods than were discussed here.


\section{Conclusions \label{sec:conclusions}}

\begin{description}
	\item[Exercise 1:]
		Working with FITS files and DS9, which are both common for professional astronomers, faces starters with considerable difficulties.
		By exploring the capabilities of the tools, combining an image taken at a wavelength of $160\mathrm{\mu m}$ with an image taken at $70\mathrm{\mu m}$ into an image with a red and blue channel was successful.
		The result is included as figure \ref{fig:rgb-image}.
		\ \\
	\item[Exercise 2:]
		Extracting the voltage bias and noise from a stray light measurement requires careful fitting of probability density functions (pdfs) to the measurement's distribution.
		It is important to account for the contamination from the stray light, when fitting a Gaussian to the boundary region of the distribution. 
		This is done by fitting an exponential to the rising signal edge of the stray light's data.
		Subtracting the estimated values using the exponential fit from the distribution yields a corrected Gaussian fit, whose location and scale parameters are the voltage bias and noise.
\end{description}


\bibliography{refs}


\begin{appendix}


\section{Additional Images for Alpha ORI}\label{app:images}
See figures \ref{fig:big-rgb} and \ref{fig:bw-comp}.

\begin{figure*}
	\centering
	
	\resizebox{.7\hsize}{!}{\includegraphics{HPPUNIMAPB_RGB}}
	\caption{Larger Version of Figure \ref{fig:rgb-image} on Page \pageref{fig:rgb-image}}
	\label{fig:big-rgb}
\end{figure*}

\begin{figure*}
	\centering
	\null\vfill\null
	\begin{subfigure}{.44\hsize}
		\resizebox{\hsize}{!}{\includegraphics[trim=60 0 60 40,clip]{HPPUNIMAPB_BW}}
		\caption{HPPUNIMAP}
		\label{subfig:unimap-bw}
	\end{subfigure}
	\quad\hfill
	\begin{subfigure}{.44\hsize}
		\resizebox{\hsize}{!}{\includegraphics[trim=60 0 60 40,clip]{HPPJSMAPB_BW}}
		\caption{HPPJSMAP}
		\label{subfig:jsmap-bw}
	\end{subfigure}
	\caption{Images used to determine which mapping method to use for the final picture (fig. \ref{fig:rgb-image}) as discussed in section \ref{subsec:img-det}. Both are taken at a wavelength of $70\mathrm{\mu m}$. Figure (\subref{subfig:unimap-bw}) is the result of the HPPUNIMAP method and (\subref{subfig:jsmap-bw}) of HPPJSMAP.}
	\label{fig:bw-comp}
\end{figure*}


\section{Python Code for Exercise 2\label{app:code-sample}}
The following Python code (fig. \ref{fig:code}) was used to solve the second exercise.
The focus of the reader should be directed to the routines for fitting a Gaussian to the CCD measurement noise.
That part can be found towards the end of the {\it``plot()''} function.

\begin{figure*}
	\resizebox{\hsize}{!}{\includegraphics[page=1,angle=-90,trim=60 60 60 60,clip]{aufgabe2-code}}
	\null\vfill\null
	\resizebox{\hsize}{!}{\includegraphics[page=2,angle=-90,trim=60 60 60 30,clip]{aufgabe2-code}}
	\caption{Python Code for Exercise 2}
	\label{fig:code}
\end{figure*}


\end{appendix}


\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Examples for figures using graphicx

%-------------------------------------------------------------
%                 A figure as large as the width of the column
%-------------------------------------------------------------
   \begin{figure}
   \centering
   \includegraphics[width=\hsize]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                                    One column rotated figure
%-------------------------------------------------------------
   \begin{figure}
   \centering
   \includegraphics[angle=-90,width=3cm]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                        Figure with caption on the right side 
%-------------------------------------------------------------
   \begin{figure}
   \sidecaption
   \includegraphics[width=3cm]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                                Figure with a new BoundingBox 
%-------------------------------------------------------------
   \begin{figure}
   \centering
   \includegraphics[bb=10 20 100 300,width=3cm,clip]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                                      The "resizebox" command 
%-------------------------------------------------------------
   \begin{figure}
   \resizebox{\hsize}{!}
            {\includegraphics[bb=10 20 100 300,clip]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure}
%
%-------------------------------------------------------------
%                                             Two column Figure 
%-------------------------------------------------------------
   \begin{figure*}
   \resizebox{\hsize}{!}
            {\includegraphics[bb=10 20 100 300,clip]{empty.eps}
      \caption{Vibrational stability equation of state
               $S_{\mathrm{vib}}(\lg e, \lg \rho)$.
               $>0$ means vibrational stability.
              }
         \label{FigVibStab}
   \end{figure*}
%
%-------------------------------------------------------------
%                                             Simple A&A Table
%-------------------------------------------------------------
%
\begin{table}
\caption{Nonlinear Model Results}             % title of Table
\label{table:1}      % is used to refer this table in the text
\centering                          % used for centering table
\begin{tabular}{c c c c}        % centered columns (4 columns)
\hline\hline                 % inserts double horizontal lines
HJD & $E$ & Method\#2 & Method\#3 \\    % table heading 
\hline                        % inserts single horizontal line
   1 & 50 & $-837$ & 970 \\      % inserting body of the table
   2 & 47 & 877    & 230 \\
   3 & 31 & 25     & 415 \\
   4 & 35 & 144    & 2356 \\
   5 & 45 & 300    & 556 \\ 
\hline                                   %inserts single line
\end{tabular}
\end{table}
%
%-------------------------------------------------------------
%                                             Two column Table 
%-------------------------------------------------------------
%
\begin{table*}
\caption{Nonlinear Model Results}             
\label{table:1}      
\centering          
\begin{tabular}{c c c c l l l }     % 7 columns 
\hline\hline       
                      % To combine 4 columns into a single one 
HJD & $E$ & Method\#2 & \multicolumn{4}{c}{Method\#3}\\ 
\hline                    
   1 & 50 & $-837$ & 970 & 65 & 67 & 78\\  
   2 & 47 & 877    & 230 & 567& 55 & 78\\
   3 & 31 & 25     & 415 & 567& 55 & 78\\
   4 & 35 & 144    & 2356& 567& 55 & 78 \\
   5 & 45 & 300    & 556 & 567& 55 & 78\\
\hline                  
\end{tabular}
\end{table*}
%
%-------------------------------------------------------------
%                                          Table with notes 
%-------------------------------------------------------------
%
% A single note
\begin{table}
\caption{\label{t7}Spectral types and photometry for stars in the
  region.}
\centering
\begin{tabular}{lccc}
\hline\hline
Star&Spectral type&RA(J2000)&Dec(J2000)\\
\hline
69           &B1\,V     &09 15 54.046 & $-$50 00 26.67\\
49           &B0.7\,V   &*09 15 54.570& $-$50 00 03.90\\
LS~1267~(86) &O8\,V     &09 15 52.787&11.07\\
24.6         &7.58      &1.37 &0.20\\
\hline
LS~1262      &B0\,V     &09 15 05.17&11.17\\
MO 2-119     &B0.5\,V   &09 15 33.7 &11.74\\
LS~1269      &O8.5\,V   &09 15 56.60&10.85\\
\hline
\end{tabular}
\tablefoot{The top panel shows likely members of Pismis~11. The second
panel contains likely members of Alicante~5. The bottom panel
displays stars outside the clusters.}
\end{table}
%
% More notes
%
\begin{table}
\caption{\label{t7}Spectral types and photometry for stars in the
  region.}
\centering
\begin{tabular}{lccc}
\hline\hline
Star&Spectral type&RA(J2000)&Dec(J2000)\\
\hline
69           &B1\,V     &09 15 54.046 & $-$50 00 26.67\\
49           &B0.7\,V   &*09 15 54.570& $-$50 00 03.90\\
LS~1267~(86) &O8\,V     &09 15 52.787&11.07\tablefootmark{a}\\
24.6         &7.58\tablefootmark{1}&1.37\tablefootmark{a}   &0.20\tablefootmark{a}\\
\hline
LS~1262      &B0\,V     &09 15 05.17&11.17\tablefootmark{b}\\
MO 2-119     &B0.5\,V   &09 15 33.7 &11.74\tablefootmark{c}\\
LS~1269      &O8.5\,V   &09 15 56.60&10.85\tablefootmark{d}\\
\hline
\end{tabular}
\tablefoot{The top panel shows likely members of Pismis~11. The second
panel contains likely members of Alicante~5. The bottom panel
displays stars outside the clusters.\\
\tablefoottext{a}{Photometry for MF13, LS~1267 and HD~80077 from
Dupont et al.}
\tablefoottext{b}{Photometry for LS~1262, LS~1269 from
Durand et al.}
\tablefoottext{c}{Photometry for MO2-119 from
Mathieu et al.}
}
\end{table}
%
%-------------------------------------------------------------
%                                       Table with references 
%-------------------------------------------------------------
%
\begin{table*}[h]
 \caption[]{\label{nearbylistaa2}List of nearby SNe used in this work.}
\begin{tabular}{lccc}
 \hline \hline
  SN name &
  Epoch &
 Bands &
  References \\
 &
  (with respect to $B$ maximum) &
 &
 \\ \hline
1981B   & 0 & {\it UBV} & 1\\
1986G   &  $-$3, $-$1, 0, 1, 2 & {\it BV}  & 2\\
1989B   & $-$5, $-$1, 0, 3, 5 & {\it UBVRI}  & 3, 4\\
1990N   & 2, 7 & {\it UBVRI}  & 5\\
1991M   & 3 & {\it VRI}  & 6\\
\hline
\noalign{\smallskip}
\multicolumn{4}{c}{ SNe 91bg-like} \\
\noalign{\smallskip}
\hline
1991bg   & 1, 2 & {\it BVRI}  & 7\\
1999by   & $-$5, $-$4, $-$3, 3, 4, 5 & {\it UBVRI}  & 8\\
\hline
\noalign{\smallskip}
\multicolumn{4}{c}{ SNe 91T-like} \\
\noalign{\smallskip}
\hline
1991T   & $-$3, 0 & {\it UBVRI}  &  9, 10\\
2000cx  & $-$3, $-$2, 0, 1, 5 & {\it UBVRI}  & 11\\ %
\hline
\end{tabular}
\tablebib{(1)~\citet{branch83};
(2) \citet{phillips87}; (3) \citet{barbon90}; (4) \citet{wells94};
(5) \citet{mazzali93}; (6) \citet{gomez98}; (7) \citet{kirshner93};
(8) \citet{patat96}; (9) \citet{salvo01}; (10) \citet{branch03};
(11) \citet{jha99}.
}
\end{table}
%-------------------------------------------------------------
%                      A rotated Two column Table in landscape  
%-------------------------------------------------------------
\begin{sidewaystable*}
\caption{Summary for ISOCAM sources with mid-IR excess 
(YSO candidates).}\label{YSOtable}
\centering
\begin{tabular}{crrlcl} 
\hline\hline             
ISO-L1551 & $F_{6.7}$~[mJy] & $\alpha_{6.7-14.3}$ 
& YSO type$^{d}$ & Status & Comments\\
\hline
  \multicolumn{6}{c}{\it New YSO candidates}\\ % To combine 6 columns into a single one
\hline
  1 & 1.56 $\pm$ 0.47 & --    & Class II$^{c}$ & New & Mid\\
  2 & 0.79:           & 0.97: & Class II ?     & New & \\
  3 & 4.95 $\pm$ 0.68 & 3.18  & Class II / III & New & \\
  5 & 1.44 $\pm$ 0.33 & 1.88  & Class II       & New & \\
\hline
  \multicolumn{6}{c}{\it Previously known YSOs} \\
\hline
  61 & 0.89 $\pm$ 0.58 & 1.77 & Class I & \object{HH 30} & Circumstellar disk\\
  96 & 38.34 $\pm$ 0.71 & 37.5& Class II& MHO 5          & Spectral type\\
\hline
\end{tabular}
\end{sidewaystable*}
%-------------------------------------------------------------
%                      A rotated One column Table in landscape  
%-------------------------------------------------------------
\begin{sidewaystable}
\caption{Summary for ISOCAM sources with mid-IR excess 
(YSO candidates).}\label{YSOtable}
\centering
\begin{tabular}{crrlcl} 
\hline\hline             
ISO-L1551 & $F_{6.7}$~[mJy] & $\alpha_{6.7-14.3}$ 
& YSO type$^{d}$ & Status & Comments\\
\hline
  \multicolumn{6}{c}{\it New YSO candidates}\\ % To combine 6 columns into a single one
\hline
  1 & 1.56 $\pm$ 0.47 & --    & Class II$^{c}$ & New & Mid\\
  2 & 0.79:           & 0.97: & Class II ?     & New & \\
  3 & 4.95 $\pm$ 0.68 & 3.18  & Class II / III & New & \\
  5 & 1.44 $\pm$ 0.33 & 1.88  & Class II       & New & \\
\hline
  \multicolumn{6}{c}{\it Previously known YSOs} \\
\hline
  61 & 0.89 $\pm$ 0.58 & 1.77 & Class I & \object{HH 30} & Circumstellar disk\\
  96 & 38.34 $\pm$ 0.71 & 37.5& Class II& MHO 5          & Spectral type\\
\hline
\end{tabular}
\end{sidewaystable}
%
%-------------------------------------------------------------
%                              Table longer than a single page  
%-------------------------------------------------------------
% All long tables will be placed automatically at the end of the document
%
\longtab{
\begin{longtable}{lllrrr}
\caption{\label{kstars} Sample stars with absolute magnitude}\\
\hline\hline
Catalogue& $M_{V}$ & Spectral & Distance & Mode & Count Rate \\
\hline
\endfirsthead
\caption{continued.}\\
\hline\hline
Catalogue& $M_{V}$ & Spectral & Distance & Mode & Count Rate \\
\hline
\endhead
\hline
\endfoot
%%
Gl 33    & 6.37 & K2 V & 7.46 & S & 0.043170\\
Gl 66AB  & 6.26 & K2 V & 8.15 & S & 0.260478\\
Gl 68    & 5.87 & K1 V & 7.47 & P & 0.026610\\
         &      &      &      & H & 0.008686\\
Gl 86 
\footnote{Source not included in the HRI catalog. See Sect.~5.4.2 for details.}
         & 5.92 & K0 V & 10.91& S & 0.058230\\
\end{longtable}
}
%
%-------------------------------------------------------------
%                              Table longer than a single page
%                                            and in landscape, 
%                    in the preamble, use: \usepackage{lscape}
%-------------------------------------------------------------

% All long tables will be placed automatically at the end of the document
%
\longtab{
\begin{landscape}
\begin{longtable}{lllrrr}
\caption{\label{kstars} Sample stars with absolute magnitude}\\
\hline\hline
Catalogue& $M_{V}$ & Spectral & Distance & Mode & Count Rate \\
\hline
\endfirsthead
\caption{continued.}\\
\hline\hline
Catalogue& $M_{V}$ & Spectral & Distance & Mode & Count Rate \\
\hline
\endhead
\hline
\endfoot
%%
Gl 33    & 6.37 & K2 V & 7.46 & S & 0.043170\\
Gl 66AB  & 6.26 & K2 V & 8.15 & S & 0.260478\\
Gl 68    & 5.87 & K1 V & 7.47 & P & 0.026610\\
         &      &      &      & H & 0.008686\\
Gl 86
\footnote{Source not included in the HRI catalog. See Sect.~5.4.2 for details.}
         & 5.92 & K0 V & 10.91& S & 0.058230\\
\end{longtable}
\end{landscape}
}
%
%-------------------------------------------------------------
%               Appendices have to be placed at the end, after
%                                        \end{thebibliography}
%-------------------------------------------------------------
\end{thebibliography}

\begin{appendix} %First appendix
\section{Background galaxy number counts and shear noise-levels}
Because the optical images used in this analysis...
\begin{figure*}%f1
\includegraphics[width=10.9cm]{1787f23.eps}
\caption{Shown in greyscale is a...}
\label{cl12301}
\end{figure*}

In this case....
\begin{figure*}
\centering
\includegraphics[width=16.4cm,clip]{1787f24.ps}
\caption{Plotted above...}
\label{appfig}
\end{figure*}

Because the optical images...

\section{Title of Second appendix.....} %Second appendix
These studies, however, have faced...
\begin{table}
\caption{Complexes characterisation.}\label{starbursts}
\centering
\begin{tabular}{lccc}
\hline \hline
Complex & $F_{60}$ & 8.6 &  No. of  \\
...
\hline
\end{tabular}
\end{table}

The second method produces...
\end{appendix}
%
%
\end{document}

%
%-------------------------------------------------------------
%          For the appendices, table longer than a single page
%-------------------------------------------------------------

% Table will be print automatically at the end of the document, 
% after the whole appendices

\begin{appendix} %First appendix
\section{Background galaxy number counts and shear noise-levels}

% In the appendices do not forget to put the counter of the table 
% as an option

\longtab[1]{
\begin{longtable}{lrcrrrrrrrrl}
\caption{Line data and abundances ...}\\
\hline
\hline
Def & mol & Ion & $\lambda$ & $\chi$ & $\log gf$ & N & e &  rad & $\delta$ & $\delta$ 
red & References \\
\hline
\endfirsthead
\caption{Continued.} \\
\hline
Def & mol & Ion & $\lambda$ & $\chi$ & $\log gf$ & B & C &  rad & $\delta$ & $\delta$ 
red & References \\
\hline
\endhead
\hline
\endfoot
\hline
\endlastfoot
A & CH & 1 &3638 & 0.002 & $-$2.551 &  &  &  & $-$150 & 150 &  Jorgensen et al. (1996) \\                    
\end{longtable}
}% End longtab
\end{appendix}

%-------------------------------------------------------------
%                   For appendices and landscape, large table:
%                    in the preamble, use: \usepackage{lscape}
%-------------------------------------------------------------

\begin{appendix} %First appendix
%
\longtab[1]{
\begin{landscape}
\begin{longtable}{lrcrrrrrrrrl}
...
\end{longtable}
\end{landscape}
}% End longtab
\end{appendix}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  list for correct label nameing scheme
%
% | ch: 	chapter
% | sec: 	section
% | subsec: 	subsection
% | fig: 	figure
% | tab: 	table
% | eq: 	equation
% | lst: 	code listing
% | itm: 	enumerated list item
% | alg: 	algorithm
% | app: 	appendix subsection
%
%
% what to abbreviate (unless at beginning of sentence):
%
% Sect., Sects., Fig., Figs., Col., Cols.
% Table is never abbreviated
%
%
% citations
%
% \citet{jon90}					⇒	Jones et al. (1990)
% \citep{jon90}					⇒	(Jones et al. 1990)
% \citep[see][]{jon90}				⇒	(see Jones et al. 1990)
% \citep[see][chap. ̃2]{jon90}		⇒	(see Jones et al. 1990, chap. 2)
%
% Multiple citations by including more than one citation key in the \cite command
% \citet{jon90,jam91}			⇒	Jones et al. (1990); James et al. (1991)
% \citep{jon90,jam91}			⇒	(Jones et al., 1990; James et al. 1991)
% \citep{jon90,jon91}			⇒	(Jones et al. 1990, 1991)
% \citep{jon90a,jon90b}		⇒	(Jones et al. 1990a,b)
%
